package com.nespresso.exercise.piramid;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PyramidOtherTest {
	
    @Test
	public void cannotCollapseLayersWithTheSameState() {
        Pyramid pyramid = new Pyramid();
        pyramid.addLayer("250 Slaves, 10 Anks");
        pyramid.addLayer("50 Slaves, 3 Anks");
        assertEquals("__X__\n"
        		   + "XXXXX", pyramid.print());
        pyramid.addLayer("150 Slaves, 6 Anks");
        assertEquals("_XXX_\n"
        			+"XXXXX", pyramid.print());
        pyramid.addLayer("150 Slaves, 6 Anks");
        assertEquals(" XXX \n"
        		   + "_XXX_\n"
        		   + "XXXXX", pyramid.print());
        pyramid.addLayer("150 Slaves, 12 Anks");
        assertEquals(" XXX \n"
        		   + " XXX \n"
        		   + "_XXX_\n"
        		   + "XXXXX", pyramid.print());
    }

}
