package com.nespresso.exercise.piramid;

import java.util.List;

public class DefaultPresenter implements Presenter {

	private final String BOUND_CHARACTER = "_";
	private final String EMPTY_BOUND_CHARACTER = " ";
	private final String HIGHT_QUALITY_PRESNTATION = "X";
	private final String NORMAL_QUALITY_PRESNTATION = "V";
	private final String LINE_SEPARATOR = "\n";
	
	
	private Quality layerQuality;
	private int roksNumber;

	public String present(List<Layer> layers) {
		StringBuilder builder = new StringBuilder();
		String lastLayerPresentation = "";
		for (Layer layer : layers) {
			String layerPresentation = presentLayer(layer, lastLayerPresentation);
			builder.append(layerPresentation);
			lastLayerPresentation = layerPresentation;
		}
		return builder.reverse().toString();
	}

	private String presentLayer(Layer layer, String lastLayerPresentation) {
		layer.populate(this);
		
		String presentation = present(HIGHT_QUALITY_PRESNTATION, NORMAL_QUALITY_PRESNTATION);

		if (lastLayerPresentation.isEmpty()) {
			return presentation;
		}

		presentation =  addFilledMissedBounds(lastLayerPresentation, presentation);
		presentation =  addEmptyMissedBounds(lastLayerPresentation, presentation);

		return LINE_SEPARATOR + presentation;
	}

	private String addEmptyMissedBounds(String lastLayerPresentation, String presentation) {
		int missedEmptyBounds = lastLayerPresentation.length() - presentation.length();
		presentation = addMissedBounds(presentation, missedEmptyBounds, EMPTY_BOUND_CHARACTER);
		return presentation;
	}

	private String addFilledMissedBounds(String lastLayerPresentation, String presentation) {
		String lastLayerPresentationWithoutBounds = lastLayerPresentation.replace(BOUND_CHARACTER, "").trim();
		int missedBounds = lastLayerPresentationWithoutBounds.length() - presentation.length();
		presentation = addMissedBounds(presentation, missedBounds, BOUND_CHARACTER);
		return presentation;
	}

	public void registerLayerQuality(Quality layerQuality) {
		this.layerQuality = layerQuality;
	}

	public void registerRoksNumber(int roksNumber) {
		this.roksNumber = roksNumber;
	}

	private String present(String hightQualityPresntation, String normalQualityPresntation) {
		String presentation = layerQuality == Quality.HIGHT ? hightQualityPresntation : normalQualityPresntation;
		String layerPresentation = "";

		int temporaryRoksNumber = roksNumber;
		while (temporaryRoksNumber > 0) {
			layerPresentation += presentation;
			temporaryRoksNumber--;
		}
		return layerPresentation;
	}

	private String addMissedBounds(String presentation, int missedBounds, String character) {
		int oneSideMissedBoundsCount = missedBounds / 2;
		String oneSideMissedBoundsPresentation = "";
		for (int oneSideMissedBound = 0; oneSideMissedBound < oneSideMissedBoundsCount; oneSideMissedBound++) {
			oneSideMissedBoundsPresentation += character;
		}
		return oneSideMissedBoundsPresentation + presentation + oneSideMissedBoundsPresentation;
	}

}
