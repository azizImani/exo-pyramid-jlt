package com.nespresso.exercise.piramid;


public class Layer {

	private Quality layerQuality;
	private int roksNumber;

	public Layer(Quality layerQuality, int roksNumber) {
		this.layerQuality = layerQuality;
		this.roksNumber = roksNumber;
	}

	/*
	 * J'ai choisi d'utiliser la 2�me solution car si nous avons une autres representation, par exemple representation avec interface graphique, 
	 * je serais oblig� d'ajouter une autre methode present
	 * Ainsi que les class de haut niveau no d�pend pas �galement des autres de bas niveau
	 */
	
	public boolean canCollapse(Layer layer) {
		boolean otherLayerIsWeak = layer.layerQuality == Quality.NORMAL;

		if (layer.roksNumber < this.roksNumber || (layer.roksNumber == this.roksNumber && otherLayerIsWeak)) {
			return true;
		}
		return false;
	}
	
	public void populate(Presenter presenter){
		presenter.registerLayerQuality(layerQuality);
		presenter.registerRoksNumber(roksNumber);
	}

}
