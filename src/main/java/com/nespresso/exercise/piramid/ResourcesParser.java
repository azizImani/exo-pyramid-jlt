package com.nespresso.exercise.piramid;

public class ResourcesParser {
	
	private final static String ARGUMENTS_SEPARATOR = ",";
	private final static String DATA_SEPARATOR = " ";
	
	public static int parseSlavesNumber(String recources) {
		String slaves = recources.split(ARGUMENTS_SEPARATOR)[0];
		String slavesNumber = slaves.split(DATA_SEPARATOR)[0];
		return Integer.parseInt(slavesNumber);
	}

	public static int parseAnks(String recources) {
		String anks = recources.split(ARGUMENTS_SEPARATOR)[1].trim();
		String anksNumber = anks.split(DATA_SEPARATOR)[0];
		return Integer.parseInt(anksNumber);
	}

}
