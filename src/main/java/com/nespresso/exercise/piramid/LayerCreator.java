package com.nespresso.exercise.piramid;

public class LayerCreator {
	private final static int REQUIRED_ANKS_FOR_HIGHT_QUALITY = 2;
	private final static int REQUIRED_SLAVES = 50;
	
	public static Layer createLayer(int slavesNumber, int anks) {
		Quality layerQuality = determinQuality(slavesNumber, anks);
		int roksNumber = calculateRoksNumber(slavesNumber);
		return new Layer(layerQuality, roksNumber);
	}
	
	private static Quality determinQuality(int slavesNumber, int anks){
		int roks = calculateRoksNumber(slavesNumber);
		int anksPerRoks = calculateAnksPerRoks(anks);
		
		boolean sufficientRequirementForHightQuality = anksPerRoks >= roks;
		
		if(sufficientRequirementForHightQuality){
			return Quality.HIGHT;
		}
		return Quality.NORMAL;
	}
	
	private static int calculateRoksNumber(int slavesNumber){
		int roks = slavesNumber / REQUIRED_SLAVES;
		return roks;
	}
	
	private static int calculateAnksPerRoks(int anks){
		int anksPerRoks = anks / REQUIRED_ANKS_FOR_HIGHT_QUALITY;
		return anksPerRoks;
	}

}
