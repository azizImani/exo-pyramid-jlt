package com.nespresso.exercise.piramid;

import java.util.List;

public interface Presenter {
	
	public String present(List<Layer> layers);

	public void registerLayerQuality(Quality layerQuality);

	public void registerRoksNumber(int roksNumber);
}
