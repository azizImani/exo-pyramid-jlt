package com.nespresso.exercise.piramid;

import java.util.ArrayList;
import java.util.List;

public class Pyramid {

	private List<Layer> layers;
	private Presenter presenter;

	public Pyramid() {
		layers = new ArrayList<Layer>();
		presenter = new DefaultPresenter();
	}

	public void addLayer(String recources) {
		int slavesNumber = ResourcesParser.parseSlavesNumber(recources);
		int anks = ResourcesParser.parseAnks(recources);
		Layer layer = LayerCreator.createLayer(slavesNumber, anks);
		addLayer(layer);
	}

	public String print() {
		return presenter.present(layers);
	}

	private void addLayer(Layer newLayer) {
		if (layers.isEmpty()) {
			layers.add(newLayer);
			return;
		}

		int lastIndex = layers.size() - 1;
		Layer lastlayer = layers.get(lastIndex);
		if (newLayer.canCollapse(lastlayer)) {
			layers.remove(lastIndex);
		}
		layers.add(newLayer);
	}

}
